# RE-TITULARING


Versión web de 'Titularing', juego tipo party game publicado en 1990.

Enlace en la [BGG](https://boardgamegeek.com/boardgame/15269/headlines-game)

Post con un [Print&Play en la BBG](https://boardgamegeek.com/thread/1672719/titularing-como-hacerlo-gracias-chema-pamundi) en el que me he basado.

Videotocho de Chema Pamundi:  https://www.youtube.com/watch?v=uM0-TPwLCj8&t=122s

## Componentes

### CARTAS DE LETRA

A x3

B x2

C x2

D x3

E x4

F x2

G x2

H x2

I x3

J x2

K x2

L x3

M x2

N x3

O x3

P x2

Q x1

R x2

S x2

T x3

U x2

V x2

Y x1

Z x2

### CARTAS DE TEMA (x1):

Actualidad

Asuntos amorosos

Cartas al Director

Convocatorias

Cultura

Deportes

Economía

Ecos de sociedad

Efemérides

El tiempo

Escándalos de famosos

Exposiciones

Horóscopo

Información de tráfico

Lotería

Música y espectáculos

Nacimientos

Necrológicas

Noticias internacionales

Noticias locales

Noticias nacionales

Noticias regionales

Ofertas de empleo

Pasatiempos

Política

Progresos científicos

Publicidad

Religión

Rumores y cotilleos

Salud y bienestar

Sucesos

Televisión y cine

Trabajo

Viajes

### ESPECIALES

* A ELECCIÓN DEL EDITOR (el editor señala cualquier tema de los existentes u otro nuevo). x3

* TRABALENGUAS, El editor coloca solo 1 carta de "Letras" en la bandeja. En este turno del juego las 4 palabras del tema deben empezar por la misma letra). x2

### CARTAS DE FRASE INICIAL DE TEMA (x1):

Anuncios clasificados; Alquileres

Anuncios clasificados; Se busca...

Anuncios clasificados; Se vende...

Aumente su...

El marido coge a su mujer...

Encuentran a una estrella de cine...

La mayoría de las mujeres prefieren...

La mayoría de los hombres prefieren...

La mujer coge a su marido...

La Tierra invadida por...

Novia celosa...

Novio celoso...

Niño prodigio...

Nueve de cada diez...

Pierde 20 kilos en tres días por...

Un hombre de 90 años se casa...

buzzer sound source: https://bigsoundbank.com/detail-1586-buzzer-4.html
