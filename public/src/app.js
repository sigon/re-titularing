
var $ = require('jquery')
// Bootstrap wants jQuery global =(
window.jQuery = $
var _ = require('lodash')
require('popper.js/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')

// Get Bootstrap styles with cssify
// var style = require('./node_modules/bootstrap/dist/css/bootstrap.min.css')

var cssify = require('cssify')
cssify.byUrl('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')

var countdownModule = require('./modules/countdown')
var NoSleep = require('nosleep.js/dist/NoSleep')

var noSleep = new NoSleep()
var mazoLetras = require('./mazos/mazoLetras')
var mazoTemas = require('./mazos/mazoTemas')

var letras // cuatro letras del mazo de letras elegidas al azar
var tema // tema del mazo de temas elegido al azar

// inicio turno
$('#barajaCartasButton').click(function (e) {
  e.preventDefault()

  $('#tema-card').removeClass('bg-danger')
  // baraja cartas
  letras = _.sampleSize(mazoLetras, 4)
  $('#letra-1').text(letras[0])
  $('#letra-2').text(letras[1])
  $('#letra-3').text(letras[2])
  $('#letra-4').text(letras[3])
  tema = _.sample(mazoTemas)
  $('#tema-txt').text(tema[1])
  $('#tema-class').text(tema[0])
  if (tema[0] === 'TRABALENGUAS') {
    trabalenguas(letras[0])
  }
  // page change to full screen and no sleep:
  openFullscreen()
  $('#tablero')[0].scrollIntoView()
  var tiempo = 60 * 1000 * 3 // tres minutos
  countdownModule(tiempo)

  // Enable wake lock:
  document.addEventListener('click', function enableNoSleep () {
    document.removeEventListener('click', enableNoSleep, false)
    noSleep.enable()
  }, false)
})

$('#exit-fullScreen').click(function (e) {
  closeFullscreen()
  // Disable wake lock
  noSleep.disable()
})

var tablero = document.documentElement
/* When the openFullscreen() function is executed, open the video in fullscreen.
Note that we must include prefixes for different browsers, as they don't support the requestFullscreen property yet */
function openFullscreen () {
  if (tablero.requestFullscreen) {
    tablero.requestFullscreen()
  } else if (tablero.mozRequestFullScreen) { /* Firefox */
    tablero.mozRequestFullScreen()
  } else if (tablero.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    tablero.webkitRequestFullscreen()
  } else if (tablero.msRequestFullscreen) { /* IE/Edge */
    tablero.msRequestFullscreen()
  }
}

/* Close fullscreen */
function closeFullscreen () {
  if (document.exitFullscreen) {
    document.exitFullscreen()
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen()
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen()
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen()
  }
}

function trabalenguas (letra) {
  $('#letra-2').text(letra)
  $('#letra-3').text(letra)
  $('#letra-4').text(letra)
}
