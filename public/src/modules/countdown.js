function countdownModule (tiempo) {
  var $ = require('jquery')

  // var tiempo = 60 * 1000 * 3 // tres minutos
  var countDownDate = new Date().getTime() + tiempo

  // Update the count down every 1 second
  var x = setInterval(function () {
    // Get today's date and time
    var now = new Date().getTime()

    // Find the distance between now and the count down date
    var distance = countDownDate - now

    // Time calculations for minutes and seconds
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
    var seconds = Math.floor((distance % (1000 * 60)) / 1000)

    // Display the result in the element with id="timer"
    $('#timer').text(minutes + 'm ' + seconds + 's ')

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x)
      $('#timer').text('EXPIRED')
      // add buzzer sound after expired timer
      var audioControls = '<audio controls="controls" autoplay="autoplay"><source src="./sounds/1586.wav" type="audio/mpeg">Your browser does not support the audio element.</audio>'
      // change color to red
      $('#tema-card').addClass('bg-danger')
      // change text
      $('#tema-txt').text('SE ACABO EL TIEMPO')
      // make sound
      $('#timer').html(audioControls)
      // scroll to 'se acabo el tiempo'
      $('html, body').animate({
        scrollTop: ($('#tema-card').offset().top)
      }, 500)
    }
  }, 1000)
}

module.exports = countdownModule
